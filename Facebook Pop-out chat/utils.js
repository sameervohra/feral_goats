const TEXT_NODE_TYPE = 3;

function onDomElementCreated(selector, callback) {    
  var total_time = 0;
  
  var recurringFunc = setInterval(function () {
    if(jQuery(selector).length > 0) {
      clearInterval(recurringFunc);
      callback();
    } else {
      total_time += DOM_LOADED_MONITORING_INTERVAL;
      if(total_time > DOM_LOADED_MONITORING_TIMEOUT) {
        clearInterval(recurringFunc);
        console.log("DOM Element :" + selector + " not found. Timeout.");
      }
    }
  }, DOM_LOADED_MONITORING_INTERVAL);   
}


function removeOnlyTextChildren(elements)
{
  elements.contents().filter(function(){ return this.nodeType == TEXT_NODE_TYPE; }).remove();
}

function replaceText(elements, text)
{
  var children = elements.contents();
  
  var text_children = children.filter(function(){ return this.nodeType == TEXT_NODE_TYPE; });
  text_children.replaceWith(text);
  
  var non_text_children = children.filter(function(){ return this.nodeType != TEXT_NODE_TYPE; })
  if(non_text_children.length > 0) {
    replaceText(non_text_children, text);
  }
}