jQuery.noConflict();

app.addPopOutButtonToChatlets = (function ($) {
  const CLOSE_BUTTON_SELECTOR = '.close';  
  const POP_OUT_BUTTON_CLASS = 'pop_out_chatlet_button';
  const POP_OUT_BUTTON_SELECTOR = '.' + POP_OUT_BUTTON_CLASS;
  const CHATLET_POP_OUT_BUTTON_HTML = '<a data-reactoot class="button enabled ' + POP_OUT_BUTTON_CLASS + '"                                               \
                                          data-hover="tooltip"                                                                      \
                                          aria-label="' + POP_OUT_CHAT_TOOLTIP + '"                                                 \
                                          style="width:25px; background-image: url(\'' + POP_OUT_ICON_CHATLET_IMAGE_URL + '\')"     \
                                          data-hover="tooltip"                                                                      \
                                          data-tooltip-content="' + POP_OUT_CHAT_TOOLTIP + '"                                       \
                                          data-tooltip-position="above"                                                             \
                                          >                                                               \
                                       </a>';

  function chatletPopoutButtonHandler() {
    var url = $(this).closest('.fbNubFlyoutTitlebar').find('.titlebarText').attr('href');
    
    if (url.match(/.facebook.com\/profile.php\?id=/)) {
      url = url.replace('.facebook.com/profile.php?id=', '.facebook.com/messages/');
    } else if (!url.match(/.facebook.com\/messages/)) {
      url = url.replace('.facebook.com', '.facebook.com/messages');
    }

    window.open(url + POP_UP_URL_POSTFIX, "_blank", get_pop_out_options());
  }

  function addPopOutButtons() {
    var chatlets = $('.fbDockChatTabFlyout .titlebarButtonWrapper');
    
    _.each(chatlets, function (chatlet) {
      var $chatlet = $(chatlet);
      
      if ($chatlet.find(POP_OUT_BUTTON_SELECTOR).length == 0) {
        var close_button = $chatlet.find(CLOSE_BUTTON_SELECTOR);
        close_button.before(CHATLET_POP_OUT_BUTTON_HTML);  
      
        $chatlet.find(POP_OUT_BUTTON_SELECTOR).on('click', chatletPopoutButtonHandler);
      }
    });
  }

  function setupChatletDomListener() {
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
    var observer = new MutationObserver(function (mutations) {
      addPopOutButtons();
    });
    observer.observe(document.querySelector('#ChatTabsPagelet'), { childList: true, characterData: true, subtree: true });
  }

  function setupChatletDomListenerOnDomConstructed() {
    onDomElementCreated('#ChatTabsPagelet', function() {
      addPopOutButtons();
      setupChatletDomListener();
    });
  }

  return {
    init: setupChatletDomListenerOnDomConstructed
  };
})(jQuery);

app.addPopOutButtonToChatlets.init();