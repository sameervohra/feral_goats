chrome.webNavigation.onHistoryStateUpdated.addListener(function(details) {
  var tabId = details.tabId;
  var url = details.url;
  var transistionType = details.transitionType;
  var transistionQualifiers = details.transitionQualifiers;
  
  console.log("Page changed to " + url + " (transisition type: " + transistionType + ", transition qualifiers = '" + transistionQualifiers + "')");
  
  chrome.tabs.sendMessage(tabId, "");
});