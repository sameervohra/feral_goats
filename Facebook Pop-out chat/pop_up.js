jQuery.noConflict();

app.PopUp = (function ($) {
  const CHAT_HEADER_BUTTONS_SELECTOR = "[aria-labelledby='webMessengerHeaderName'] button";
  const CHAT_BOX_BUTTONS_SELECTOR = ".uiButton .uiButtonText";
  const SHOW_NAME_LIST_BUTTON_IMAGE = chrome.extension.getURL("images/show.png");
  const HIDE_NAME_LIST_BUTTON_IMAGE = chrome.extension.getURL("images/hide.png");
  const TOGGLE_BUTTONS_HTML = '<div class="toggle_buttons_wrapper">                                         \
                                  <span class="_5ugh _3z_5" id="unread_counter"></span>                     \
                                  <div id="toggle_buttons" >                                                \
                                      <img id="show_button" src="' + SHOW_NAME_LIST_BUTTON_IMAGE + '">      \
                                      <img id="hide_button" src="' + HIDE_NAME_LIST_BUTTON_IMAGE + '">      \
                                  </div><span class="jewelCount">                                           \
                               </div>';
  const INVISIBLE_TABLE_CELL_HTML = '<div id="invisible_table_cell"></div>';
  const NEW_CHAT_COUNTER_CONTAINER_SELECTOR = '.selectedFolder';
  const TITLE_SELECTOR = 'head > title';
  
  var $NEW_CHAT_COUNTER_CONTAINER;
  var $UNREAD_COUNTER;
  
  var unread_messages_count = 0;
  var is_name_list_hidden = true;
  
  
  function initialize() {
    onDomElementCreated('.wmMasterView', setupChat);
  }

  function setupChat() {
    var $body = $('body');
    $body.css({"overflow": "auto"});
    
    var $page_root = $body.find('._li');
    var $web_messenger = $('#pagelet_web_messenger');
    $NAME_LIST= $('.wmMasterView');
    var $chat_area = $NAME_LIST.next();
    var $chat_area_parent = $chat_area.parent()
    
    $chat_area.after(INVISIBLE_TABLE_CELL_HTML);

    $chat_area_parent.addClass('chat_area_parent');
    $NAME_LIST.addClass('name_list');    
    $chat_area.addClass('chat_area');
    
    $chat_area.addClass('force_full_width');
    $chat_area.find('.uiScrollableArea').addClass('force_full_width');
    $chat_area.find('.uiScrollableAreaBody').addClass('force_full_width');
        
    var $emoticons_panel = $chat_area.find('.emoticonsPanel').parent();
    $emoticons_panel.addClass('emoticons_panel');
        
    removeButtonText();
    
    // Remove all content except the chat
    $page_root.replaceWith($web_messenger);
    
    setupNameListToggleButtons();
    setupUnreadCounterMutationObserver();
    setupTitleMutationObserver();
    
    // Set initial state of name list
    //is_name_list_hidden = !($.cookie(POP_UP_CHAT_SHOW_NAME_LIST_COOKIE_NAME) == "true");
    updateShowHideButtons(is_name_list_hidden);

    // Force FB to resize window again.
    // This is a temporary fix for a resizing issue where the pop-up content is larger than the pop-up window and you need to scroll to see the message box
    window.resizeBy(0, 1);

    //$(window).on('resize', _.debounce(saveWindowState, 100));
    fixEmoticonPanelLocation();
  }
  
  function fixEmoticonPanelLocation() {
    $emoticons_panel = jQuery('.emoticons_panel');
    
    $panelFlyout = $emoticons_panel.find('.panelFlyout');
    $panelFlyout.css({"right":"0"});

    $arrow = $emoticons_panel.find('.panelFlyoutArrow');
    $arrow.css({"right":"4px"});
    $arrow.css({"left":"auto"});
  }
  
  function removeButtonText() { 
    $(CHAT_BOX_BUTTONS_SELECTOR).remove();
    
    $header_buttons = $(CHAT_HEADER_BUTTONS_SELECTOR);
    removeOnlyTextChildren($header_buttons);
  }
  
  function saveWindowState() {  
    $.cookie(POP_UP_CHAT_WIDTH_COOKIE_NAME, $(window).width(), { expires: 999, path: '/' });
    $.cookie(POP_UP_CHAT_HEIGHT_COOKIE_NAME, $(window).height() - 1, { expires: 999, path: '/' });
    $.cookie(POP_UP_CHAT_SHOW_NAME_LIST_COOKIE_NAME, !is_name_list_hidden, { expires: 999, path: '/' });
  }

  function updateUnreadCounter() {
    var counter = $NEW_CHAT_COUNTER_CONTAINER.text().match(/\d+/);

    if ($NEW_CHAT_COUNTER_CONTAINER.find('.hidden_elem').length == 0) {
      $UNREAD_COUNTER.text(counter);
      $UNREAD_COUNTER.show();
      unread_messages_count = counter;
    } else {
      $UNREAD_COUNTER.hide();
      unread_messages_count = 0;
    }
  }
  
  function setupUnreadCounterMutationObserver() {
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
    var observer = new MutationObserver(function (mutations) {
      updateUnreadCounter();
    });

    onDomElementCreated(NEW_CHAT_COUNTER_CONTAINER_SELECTOR, function() {
      updateUnreadCounter();
      observer.observe(document.querySelector(NEW_CHAT_COUNTER_CONTAINER_SELECTOR), { attributes: true, childList: true, subtree: true });  
    });
  }
  
  function setupTitleMutationObserver() {
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
    var observer = new MutationObserver(_.debounce(updateTitle, 800, true));

    observer.observe(document.querySelector(TITLE_SELECTOR), { characterData: true, childList: true, subtree: true });  
  }
  
  function updateTitle(mutations) {
    var title = document.title;

    if(title.match(/\(\d+\) /)) {
      unread_messages_count_string = '';
        
      if(unread_messages_count > 0) {
        unread_messages_count_string = '(' + unread_messages_count + ') '
      }
        
      document.title = title.replace(/\(\d+\) /, unread_messages_count_string);
    }
  }
  
  function setupNameListToggleButtons() {
    $('#webMessengerHeaderName').parent().prepend(TOGGLE_BUTTONS_HTML);
    $('#show_button').hide();
    $('#toggle_buttons').click(function (event) {
      toggleButtonClickHandler(event);
    });

    //Shorten existing buttons
    var chat_header = jQuery("[aria-labelledby='webMessengerHeaderName']");
    var buttons = chat_header.find('.uiButtonGroup');
    buttons.find('.firstItem input').val(''); //Remove New Message Text
    buttons.find('.selectorItem .uiButtonText').html(''); // Remove Actions Text

    $UNREAD_COUNTER = $('#unread_counter');
    $NEW_CHAT_COUNTER_CONTAINER = $(NEW_CHAT_COUNTER_CONTAINER_SELECTOR)
    updateUnreadCounter();
  }
  
  function toggleButtonClickHandler(event) {
    event.preventDefault();
    updateShowHideButtons(!is_name_list_hidden);
  }

  function updateShowHideButtons(hide_name_list) {
    if(hide_name_list) {
      $('#hide_button').hide();
      $('#show_button').show();
      $NAME_LIST.hide();
    } else {
      $('#show_button').hide();
      $('#hide_button').show();
      $NAME_LIST.show();
    }

    if(hide_name_list != is_name_list_hidden) {
      widthIncrement = hide_name_list ? -POP_UP_CHAT_NAME_LIST_WIDTH : POP_UP_CHAT_NAME_LIST_WIDTH;
      window.resizeBy(widthIncrement, 0);
    }

    is_name_list_hidden = hide_name_list;
  }
	
  return {
    init: initialize
  };
})(jQuery);

app.PopUp.init();
