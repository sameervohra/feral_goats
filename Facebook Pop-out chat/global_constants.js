const POP_OUT_ICON_IMAGE_URL = chrome.extension.getURL("images/pop-out.png");
const POP_OUT_ICON_CHATLET_IMAGE_URL = chrome.extension.getURL("images/pop-out-chatlet.png");
const POP_OUT_CHAT_TOOLTIP = 'Pop Out Chat';
const POP_UP_URL_POSTFIX = '?type=popup';

const POP_UP_CHAT_NAME_LIST_WIDTH = 265;
const POP_UP_CHAT_DEFAULT_WIDTH = 530; // Matches actual messages page width on Curren's laptop
const POP_UP_CHAT_DEFAULT_HEIGHT = 620;

const POP_UP_CHAT_WIDTH_COOKIE_NAME = 'facebook_chat_pop_outs_width';
const POP_UP_CHAT_HEIGHT_COOKIE_NAME = 'facebook_chat_pop_outs_height';
const POP_UP_CHAT_SHOW_NAME_LIST_COOKIE_NAME = 'facebook_chat_pop_outs_show_name_list';

const DOM_LOADED_MONITORING_TIMEOUT = 15000; 
const DOM_LOADED_MONITORING_INTERVAL = 100; 

app = {};

function get_pop_out_options() {
    var width = jQuery.cookie(POP_UP_CHAT_WIDTH_COOKIE_NAME) || POP_UP_CHAT_DEFAULT_WIDTH;
    var height = jQuery.cookie(POP_UP_CHAT_HEIGHT_COOKIE_NAME) || POP_UP_CHAT_DEFAULT_HEIGHT;
    
    return "width=" + width + ", height=" + height;
}
