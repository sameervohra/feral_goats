jQuery.noConflict();

app.addPopOutButton = (function ($) {
  const CHAT_HEADER_BUTTONS_CONTAINER_SELECTOR = "[aria-labelledby='webMessengerHeaderName'] .rfloat .rfloat"; 
  const POP_OUT_BUTTON_ID = 'pop_out_button';
  const POP_OUT_BUTTON_SELECTOR = '#' + POP_OUT_BUTTON_ID;

  var is_waiting_for_dom_construction = false;
  
  function setupPopOutButton() {
    // Find first button, clone it, then change text and image
    // Button was added by cloning so that the look stays consistent with others even if FB changes it's button styling
    var first_chat_header_button = $(CHAT_HEADER_BUTTONS_CONTAINER_SELECTOR).children().first(":visible");
    var last_chat_header_button = $(CHAT_HEADER_BUTTONS_CONTAINER_SELECTOR).children().last(":visible");
    
    var pop_out_button = first_chat_header_button.clone();
    var pop_out_button_image = pop_out_button.find("i");
        
    pop_out_button.attr("id", POP_OUT_BUTTON_ID)
    replaceText(pop_out_button, "Pop-Out");
    pop_out_button_image.css('background-image', 'url(' + POP_OUT_ICON_IMAGE_URL + ')');
    
    last_chat_header_button.before(pop_out_button);
    
    pop_out_button.click(popOutButtonClickHandler);
  }

  function shouldSetupPopOutButton() {
    return !is_waiting_for_dom_construction && location.href.match(/.facebook.com\/messages/) && $(POP_OUT_BUTTON_SELECTOR).length == 0;
  }

  function popOutButtonClickHandler(event) {
    event.preventDefault();

    var pop_up_url = window.location.pathname + POP_UP_URL_POSTFIX;
    window.open(pop_up_url, "_blank", get_pop_out_options());
  }

  function setupPopOutButtonOnDomConstructed() {
    if(shouldSetupPopOutButton())
    {
      is_waiting_for_dom_construction = true;
      onDomElementCreated(CHAT_HEADER_BUTTONS_CONTAINER_SELECTOR, function() {
        is_waiting_for_dom_construction = false;
        setupPopOutButton();
      });
    }
  }
  
  return {
    init: setupPopOutButtonOnDomConstructed,    
    onUrlChangeEventHandler: setupPopOutButtonOnDomConstructed
  };
})(jQuery);

app.addPopOutButton.init();

chrome.runtime.onMessage.addListener(function(request, sender){
  app.addPopOutButton.onUrlChangeEventHandler();
});
